import React, {useState, useEffect} from 'react';
import moment from 'moment';
import reactDOM from 'react-dom';
import { func } from 'prop-types';
import { functionTypeAnnotation } from '@babel/types';

moment.locale('pt-br')

function Timepicker() {
  const now = moment()
  const [hour, setHour] = useState(now.hour())
  const [minute, setMinute] = useState(now.minute())
  const [second, setSecond] = useState(now.second())
  const opcoesMinuto = [
    {num: 0, str:"00" },
    {num: 1, str:"01" },
    {num: 2, str:"02" },
    {num: 3, str:"03" },
    {num: 4, str:"04" },
    {num: 5, str:"05" },
    {num: 6, str:"06" },
    {num: 7, str:"07" },
    {num: 8, str:"08" },
    {num: 9, str:"09" },
    {num: 10, str:"10" },
    {num: 11, str:"11" },
    {num: 12, str:"12" },
    {num: 13, str:"13" },
    {num: 14, str:"14" },
    {num: 15, str:"15" },
    {num: 16, str:"16" },
    {num: 17, str:"17" },
    {num: 18, str:"18" },
    {num: 19, str:"19" },
    {num: 20, str:"20" },
    {num: 21, str:"21" },
    {num: 22, str:"22" },
    {num: 23, str:"23" },
    {num: 24, str:"24" },
    {num: 25, str:"25" },
    {num: 26, str:"26" },
    {num: 27, str:"27" },
    {num: 28, str:"28" },
    {num: 29, str:"29" },
    {num: 30, str:"30" },
    {num: 31, str:"31" },
    {num: 32, str:"32" },
    {num: 33, str:"33" },
    {num: 34, str:"34" },
    {num: 35, str:"35" },
    {num: 36, str:"36" },
    {num: 37, str:"37" },
    {num: 38, str:"38" },
    {num: 39, str:"39" },
    {num: 40, str:"40" },
    {num: 41, str:"41" },
    {num: 42, str:"42" },
    {num: 43, str:"43" },
    {num: 44, str:"44" },
    {num: 45, str:"45" },
    {num: 46, str:"46" },
    {num: 47, str:"47" },
    {num: 48, str:"48" },
    {num: 49, str:"49" },
    {num: 50, str:"50" },
    {num: 51, str:"51" },
    {num: 52, str:"52" },
    {num: 53, str:"53" },
    {num: 54, str:"54" },
    {num: 55, str:"55" },
    {num: 56, str:"56" },
    {num: 57, str:"57" },
    {num: 58, str:"58" },
    {num: 59, str:"59" }]
  const opcoesHora = [
    { num: 0, str: "00" },
    { num: 1, str: "01" },
    { num: 2, str: "02" },
    { num: 3, str: "03" },
    { num: 4, str: "04" },
    { num: 5, str: "05" },
    { num: 6, str: "06" },
    { num: 7, str: "07" },
    { num: 8, str: "08" },
    { num: 9, str: "09" },
    { num: 10, str: "10" },
    { num: 11, str: "11" },
    { num: 12, str: "12" },
    { num: 13, str: "13" },
    { num: 14, str: "14" },
    { num: 15, str: "15" },
    { num: 16, str: "16" },
    { num: 17, str: "17" },
    { num: 18, str: "18" },
    { num: 19, str: "19" },
    { num: 20, str: "20" },
    { num: 21, str: "21" },
    { num: 22, str: "22" },
    { num: 23, str: "23" }
  ];
  const hourNow = moment().hour();
  const minuteNow = moment().minute();
  const [minuteSelected, setMinuteSelected] = useState("");
  const [hourSelected, setHourSelected] = useState("");
  const [mostrarSelect, setMostrarSelect] = useState(false);

  function mostrarSelectInput() {
    setMostrarSelect(true)
  }
  function esconderSelectInput() {
    setMostrarSelect(false)
  }

  function chooseHour(event) {
    setHourSelected(event.target.value);
  }

  function chooseMinute(event) {
    setMinuteSelected(event.target.value);
  }

  const updateSecond = () => {
    setTimeout(() => {
      
      setSecond(second => second + 1)
    }, 1000)
  }

  const updateHour = () => {
    setTimeout(() => {

      setHour(hour+1)
    }, 1000)
  }

  useEffect(() => {
    updateSecond()
  }, [second])

  useEffect(() => {
    updateHour()
  }, [minute])

  // const getUltimaHora = (hour,minute) => {
  //   let time = moment({hour, minute, second});
  //   time = time.add(1, 'hour')
  //   time = time.subtract(1, 'minute')
  //   return Number(time.format('MM'))
  // }
  
  // const updateMinute = (dif) => {
  //   if(minute === 0 && dif === -1) {
  //     setSecond(second-1)
  //     setMinute(59)
  //   } else if(minute === 59 && dif === 1) {
  //     setSecond(second+1)
  //     setMinute(0)
  //   } else {
  //     setMinute(minute+dif)
  //   }
  // }
  
  return(
    <div>
    <div> 
      {<input type = "text" value= {`${hourSelected ? hourSelected : hourNow} : ${minuteSelected ? minuteSelected : minuteNow}`}
      onFocus ={mostrarSelectInput} onChange={() => {}}/>}
    </div>
    <div>

   {mostrarSelect&&
   <>
   <select
      value={hourSelected ? hourSelected : hourNow}
      onChange = {event => chooseHour(event)}
    >
      {opcoesHora.map(h => (
        <option key = {h.num} value = {h.num}>
          {h.str}
        </option>
      ))}
    </select>
    <select
      value = {minuteSelected ? minuteSelected : minuteNow}
      onChange = {event => chooseMinute(event)}
    >
      {opcoesMinuto.map(h => (
        <option key = {h.num} value = {h.num}>
          {h.str}
        </option>
        ))}
      </select>
      </>
      }
      
    </div>
    
  </div>
  )
}

export default Timepicker;